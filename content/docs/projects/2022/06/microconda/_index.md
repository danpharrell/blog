---
weight: 1
title: "micromamba error"
tags: [
    "micromamba",
    "conda",
    "c++",
    "debugging",
]
---

# Problem

I recently started using a combination of conda and neovim to manage my
development environments.  Overall this has worked relatively well, with a
slight amount of hacking to get working, but I've started to realize that conda
is just... so slowww... On average it takes about a second to run commands in
different environments, which can get a little ridiculous.


I think down the line, I'll need to use some sort of docker workflow to better
maintain my environments.  But until then, I've decided that I'd like to use
`micromamba` as my daily driver... Even though the documentation specifically
says that you shouldn't:

```
README excerpt:

Note: Micromamba is currently experimental and it's advised to use micromamba
in containers & CI only.
```

All was going well with my initial `micromamba` setup, but then I started
getting some weird errors on a Mac machine.  As an example, I was getting 
`Invalid instruction` errors whenever running `micromamba run`, and I would get
other errors too during `micromamba update` like:

```
warning  libmamba Could not find macOS version by calling 'sw_vers -productVersion'
    Please file a bug report.
    Error: Invalid argument
warning  libmamba osx version not found (virtual package skipped)
```

And that's where the rabbit hole begins.

# Diagnosing the problem

I really should write more __during__ the storm, instead of mostly
retrospectively...  I will mention though, that I didn't see any problem on
linux, and I mostly share env configs between my linux and mac machines.
Moving forward: a quick clone and grep of the mamba code brings me directly to a
very unsuspicious chunk:

```
libmamba/src/core/util_os.cpp:255
  std::string out, err;
  // Note: we could also inspect /System/Library/CoreServices/SystemVersion.plist which is
  // an XML file
  //       that contains the same information. However, then we'd either need an xml
  //       parser or some other crude method to read the data
  std::vector<std::string> args = { "sw_vers", "-productVersion" };
  auto [status, ec] = reproc::run(
      args, reproc::options{}, reproc::sink::string(out), reproc::sink::string(err));

  if (ec)
  {
      LOG_WARNING
          << "Could not find macOS version by calling 'sw_vers -productVersion'\nPlease file a bug report.\nError: "
          << ec.message();
      return "";
  }
```

Okay, so they're using a library to make system calls and capture the stdout.
Seems like pretty simple stuff.  Nothing about it seems extra-ordinary, and
since no one else has seen this issue before - it must be me :'(.

So there's a few ways to go about this:
1.  Download and build micromamba from source
  - This is ok, but very complex, I don't want to muck around in micromamba
    source forever when I can see that the problem is outside of micromamba
2.  Download and build reproc from source
  - This is also ok, but much less complex.  However this has the downside of
    not exactly mimicing the reproc version that micromamba is built upon.

So while 2 seemed like the best option, I chose 1.  Why? Because the install
scripts for micromamba seemed very simple, all managed by a few commands for
conda/mamba/micromamba.  Unfortunately, because of some weird MacOS ssl
[shenanagins](https://github.com/mamba-org/mamba/issues/1753#issuecomment-1161913033)
the macos build didn't work immediately off the bat.  Thankfully there was an
issue posted shortly before I actually encountered this problem, with the
correctl libcurl version to pin for the mamba/ subrepos.


## Attempt to fix # 1

After checking out the function, I just decided to add some generic code just
above the call to the sw_vers command. I was hoping it would simply test if
anything would be captured or printed:


```
  {
    std::string out, err;
    reproc::sink::string outt(out);
    reproc::sink::string errt(err);
    std::vector<std::string> args = { "/bin/echo", "hixx" };
    LOG_WARNING << "echohiy";
    auto [status, ec] = reproc::run(
        args, reproc::options{}, outt, errt);
      std::cout
          << "out: "
          << out.size() << "\n"
          << "err: "
          << err.size() << "\n"
          << int(bool(ec)) << std::endl;
  }

```
This gave:
```
warning  libmamba echohiy                                                                                           
out: 0                                                                                                              
err: 0
1
```

how dissapointing.

## Attempt to fix # 2

Move directly to cloning reproc and testing it alone.  Really simple stuff, just
take the example from the last attempt, slam it into the example code, and see
what happens.  So I ended up with this little diff:

```
diff --git a/reproc++/examples/run.cpp b/reproc++/examples/run.cpp
index b88c595..21dcf9b 100644
--- a/reproc++/examples/run.cpp
+++ b/reproc++/examples/run.cpp
@@ -1,10 +1,26 @@
 #include <iostream>
+#include <vector>
+#include <string>
 
 #include <reproc++/run.hpp>
 
 // Equivalent to reproc's run example but implemented using reproc++.
 int main(int argc, const char **argv)
 {
+    {
+          std::string out, err;
+          reproc::sink::string outt(out);
+          reproc::sink::string errt(err);
+          std::vector<std::string> args = { "/bin/echo", "hixx" };
+          auto [status, ec] = reproc::run(
+              args, reproc::options{}, outt, errt);
+            std::cout
+                << "out: "
+                << out.size() << "\n"
+                << "err: "
+                << err.size() << "\n"
+                << int(bool(ec)) << std::endl;
+        }
   (void) argc;
 
   int status = -1;
@@ -14,6 +30,7 @@ int main(int argc, const char **argv)
   options.redirect.parent = true;
   options.deadline = reproc::milliseconds(5000);
 
+
   std::tie(status, ec) = reproc::run(argv + 1, options);
 
   if (ec) {
```


again... I recieved a dissapointing result:
```
$ ./build/reproc++/examples/run /bin/echo xx
out: 0
err: 0
1
Invalid argument
```

## Attempt to fix # 3

This is the point where you would normally message the maintainer with a brief
github issue, and hopefully they might have an idea of what's wrong.  However,
with issues like these, it's almost always a 'works for me and the tests pass
so you're wrong' scenario.  So, what's the cure?  Well you would think this
would be a simple 5 minute task for GDB, wouldn't you say so?

### Siderant -- MacOS
Installing gdb on Macs is just... actually sort of outrageous, the security
settings, the rebooting, the searching for random github snippets that might
contain more information w/r/t your 'newer' os...  I don't want to go too much
into this, because I'm not a security expert, but if it takes this long and this
much effort to just install GDB, I don't know how anyone at the company actually
debugs anything!

### Back on topic

Even after installing gdb as best I could... Maybe I need another restart or
something, but as of right now - running my modified example app in gdb just
hangs.  I think I will give up on the easy route.


## Attempt to fix # 4

And now, once the heavy hitters have gone to sleep, `printf` can shine in
the night sky like he was always meant to.

and so the prinfs begin to fly... Adding many print statements, with
unintelligible garbage followed by numbers scattered througout every function.

The final trace was (approximately):
```
reproc++/run.hpp -> reproc++/reproc.hpp -> reproc++/reproc.cpp ->
reproc/include/reproc/reproc.h -> reproc/src/reproc.c -> ->
reproc/reproc/src/process.posix.c
```

Where I finally ended up with the beautiful stdout:


```
hix111x
hix222x
options?
go?
hi?
hi2?
hi3?
hi4?
hi5?
hi6?
YY1
YY2
YY2
YY3
YY4
YY5
YY6
YY7
YY8
QQ1
QQ2
QQ3
QQ4
QQ5
QQ6
QQ6
QQ7
QQ8
QQ9
YY9
got?
hix333x
out: 0
err: 0
1
hix111x
hix222x
options?
go?
hi?
hi2?
got?
hix333x
Invalid argument

```

This was exaclty what I wanted, because at this point I was low
level enough that there was no where else to go!  Additionally, I didn't see
QQ10, which means I was finally at wherever the issue is.

Here's a little peak at the code in this area:

```
  printf("QQ9\n");

  r = signal_mask(SIG_SETMASK, &mask.new, NULL);
  if (r < 0) {
    goto finish;
  }
  printf("QQ9.1\n");

  // Not all file descriptors might have been created with the `FD_CLOEXEC`
  // flag so we manually close all file descriptors to prevent file descriptors
  // leaking into the child process.

  r = get_max_fd();
  if (r < 0) {
    goto finish;
  }
  printf("QQ9.2\n");

  int max_fd = r;
  printf("QQ9.3 %i / %i\n", max_fd, MAX_FD_LIMIT);

  if (max_fd > MAX_FD_LIMIT) {
    // Refuse to try to close too many file descriptors.
    r = -EMFILE;
    goto finish;
  }
  printf("QQ10\n");

```

and printing it addss the stdout:

```
QQ9.1
QQ9.2
QQ9.3 2147483647 / 1048576

```

As you can see, for some reason (I'm sure it's a good reason!) the author has decided to obtain the maximum
possible number of file descriptors, and then destroy everything with the <=# or
lower further down in the function, or fail if the maximum number of file descriptors is >1024^2.
Unfortunately, you can also see that our above printed output the left hand side is the classic INT_MAX value.
And of course, at this point I knew exactly what was wrong.

# Solution and background

The following is a small excerpt from my `.zshrc`.  I had set this many years
ago when working extensively with the dask distributed computing toolkit.  I was
finding that on very large servers I was hitting the file_descriptor limit, and
many other limits as well, so to move on quickly I just added these lines to my
environment to fix all of my dask-related problems.

```
ulimit -u unlimited
ulimit -s unlimited
ulimit -n unlimited

```

Unsetting those lines in my `.zshrc`, and restarting a new shell yields:

```
hix111x
hix222x
options?
go?
hi?
hi2?
hi3?
hi4?
hi5?
hi6?
YY1
YY2
YY2
YY3
YY4
YY5
YY6
YY7
YY8
QQ1
QQ2
QQ3
QQ4
QQ5
QQ6
QQ6
QQ7
QQ8
QQ9
QQ9.1
QQ9.2
QQ9.3255/1048576
QQ10
YY9
YY10
YY15
YY16
YY17
hi7?
got?
hix333x
hix444x
???


hix555x
hix666x
out:15
err:0
0
hix111x
hix222x
options?
go?
hi?
hi2?
got?
hix333x
Invalidargument

```

Beautiful isn't it?! `out:15` is exactly what we wanted. although it seems slightly
strange that that the stdout size includes the `/bin/echo` call as well.

Oh well... I don't use dask very much anymore, and I still haven't really dug into the
reproc code enough to know why we need to try to kill everything, but that's where I
plan to leave it up to the maintainer!

# Finale

I can also confirm that resetting my file descriptor limit to 256 does indeed allow
micromamba to work correctly! On to the next problem then I guess.

---
title: Introduction
type: docs
---

# A blog about something

A history of my various thoughts.

## Things I am passionate about seeing before I die

### Science related

- Barnacles!
  - Protein based materials are very cool
  - I want barnacle cement produced from secreted proteins of two cell lines
  - I want a barnacle cement dissolving enzyme

- Protein ML Baby Projects

  - Most of the good projects require lots of gpus and lots of data... I have neither, Heres a few ideas that will hopefully be more attainable on my own

    - 2D ML-Dock
      - Just make a very simple app that takes a shape and places it in a canvas

    - Predicting AA from density
      - Just make a very simple app that takes perfect density, and predicts a sequence (small peptides)

    - Chain Tracing
      - Just make a very simple app that takes perfect density, and chases the backbone

    - Transform Peptides
      - Just make a very simple app that takes perfect density and a peptide, transforms the peptide into the density

- An open-source, free-for-commercial, real-time-collaborative alternative to ucsf-chimera


### Non-science related

- A better matplotlib
  - It has too many API specific gotchas.
  - The interactive frontends are slow

- An open-source, real-time-collaborative alternative to the original onenote

- A nice alternative to cmake
